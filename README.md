[![CircleCI](https://circleci.com/bb/rambrogi/traffic-meister.svg?style=svg)](https://circleci.com/bb/rambrogi/traffic-meister)

# TrafficMeister

![TrafficMeister](/images/screenshot.png)

## How does it works

### [Demo](https://dazzling-mcnulty-f0bf95.netlify.com/)

TrafficMeister allows you to choose a vehicle (car, airplane and train) based on a color. So first you need to pick a color from the dropdown. Then, a dropdown for the vehicle type and vehicle brand will be show. If a type doesn't have any vehicle for that selected color, the type will not be show. Also, if the brand colors doesn't match with the selected color, it also will not be show.
If you selected a vehicle type, only brands for that type will be show. And vice-versa.

## Getting Started

```binbash
$ git clone https://rambrogi@bitbucket.org/rambrogi/traffic-meister.git
$ cd traffic-meister && npm install
```

### Running in development mode:

```binbash
$ npm start
```

##### A browser tab/window should be opened. If not, open `http://localhost:3000/` on the browser.

### Running in production mode:

```binbash
$ npm run build
$ yarn global add serve && serve -s build
```

##### Open `http://localhost:5000/` on the browser.

### Running the tests:

```binbash
$ npm run test
```

## Development

### Framework

*   ReactJS (^16.3 pre release)

### Tools

*   ### UI
    *   react-gizmo
    *   recompose
    *   ramda
    *   semantic-ui
*   ### Tests
    *   Jest
    *   Enzyme

## Info

*   Documentation can be found inside the `/docs` folder. Just open the `index.html` in your browser.
*   This project uses [`react-gizmo`](https://github.com/KadoBOT/react-gizmo) a finite state machine developed by me. Don't be frightened 😎
    *   Flow graph and flow log are turned off for this project. But you can turn them on by adding the `graph=bool` and `log=bool` props in the `<Machine>` component in the main `index.js` file.
