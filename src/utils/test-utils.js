export const mockData = new Map();
const vehicles = new Map();
const types = new Map();
const colors = new Set(["red", "black", "brown", "blue"]);
vehicles.set(99, {
	brand: "f40",
	id: 99,
	colors: ["white", "black", "red"],
	type: "car"
});
vehicles.set(123, {
	brand: "737",
	id: 123,
	colors: ["black", "white"],
	type: "airplane"
});
types.set("car", new Set(["red", "black"]));

mockData.set("vehicles", vehicles);
mockData.set("types", types);
mockData.set("colors", colors);
