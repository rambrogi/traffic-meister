import api from "../api";

/**
 * creates a relationship between the types and
 * valid colors
 *
 * @param {object} object - { colorsSet, typesMap, colors, type } Create a new Map/Set for the given attributes
 */
function setColorsAndTypes({ colorsSet, typesMap, colors, type }) {
	for (const color of colors) {
		colorsSet.add(color);
		typesMap.has(type)
			? typesMap.set(type, new Set([...typesMap.get(type), color]))
			: typesMap.set(type, new Set([color]));
	}
}

/**
 * creates an object with types[colors], [colors] and
 * the default vehicles
 *
 * @param {object} data - The API response
 * @returns object
 */
function loopItems(data) {
	const [types, colors, vehicles] = [new Map(), new Set(), new Map()];

	for (const obj of data) {
		vehicles.set(obj.id, obj);
		setColorsAndTypes({
			colorsSet: colors,
			typesMap: types,
			colors: obj.colors,
			type: obj.type
		});
	}

	return { types, colors, vehicles };
}

/**
 * creates a MAP which later will be used by the
 * App
 *
 * @param {object} object - {vehicles, colors, types} The Formatted API response
 * @returns object
 */
function setMap({ vehicles, colors, types }) {
	let map = new Map();

	map.set("vehicles", vehicles);
	map.set("colors", colors);
	map.set("types", types);

	return map;
}

export default () =>
	new Promise((resolve, reject) => {
		api.fetchData((err, data) => {
			if (err) reject(err);
			else {
				const items = loopItems(data);
				const map = setMap(items);

				resolve(map);
			}
		});
	});
