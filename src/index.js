import React from "react";
import ReactDOM from "react-dom";
import { Machine } from "react-gizmo";
import "semantic-ui-css/semantic.min.css";

import App from "./components";

const state = {
	initialState: {
		color: "",
		brand: "",
		types: "",
		image:
			"https://cdn.importantmedia.org/planetsave/uploads/2011/12/26090623/traffic.jpg"
	},
	flow: {
		initial: "loading",
		states: {
			loading: {
				onEntry: "fetchData",
				on: { SUCCESS: "colors", ERROR: "error" }
			},
			colors: {
				on: { NEXT: "brandsAndTypes", RESET: "loading" }
			},
			brandsAndTypes: {
				on: { SELECT: "brandsAndTypes", RESET: "loading" }
			},
			error: {
				on: { RETRY: "loading" }
			}
		}
	}
};

const MachineApp = () => (
	<Machine state={state}>
		<App />
	</Machine>
);

ReactDOM.render(<MachineApp />, document.getElementById("root"));
