import React, { Component, Fragment } from "react";
import { func } from "prop-types";
import { Dropdown } from "semantic-ui-react";

import "./style.css";
import Image from "../Image";
import data from "../../utils/data";

/**
 * Component that is displayed while the
 * promise is running
 *
 * @class Loading
 * @extends {Component}
 */
class Loading extends Component {
	static propTypes = {
		transition: func
	};

	/**
	 * method specified on state machine
	 * that is called onEntry to get data
	 * from promise
	 *
	 * @memberof Loading
	 */
	async fetchData() {
		try {
			this.transitionSuccess(await this.getData());
		} catch (err) {
			this.transitionFail(err);
		}
	}

	/**
	 * if fetching fails, transition to
	 * 'ERROR'.
	 *
	 * @param {string} errorMsg - The error message
	 * @memberof Loading
	 */
	transitionFail(errorMsg) {
		const options = this.options({ errorMsg });
		this.props.transition("ERROR", options);
	}

	/**
	 * get data from the api and return obj
	 * with results
	 *
	 * @returns object
	 * @memberof Loading
	 */
	async getData() {
		const options = this.options({ data: await data() });
		return options;
	}

	/**
	 * transition to SUCCESS if fetching
	 * succeeds
	 *
	 * @memberof Loading
	 */
	transitionSuccess = options => this.props.transition("SUCCESS", options);

	options = setState => ({
		setState,
		off: "loading"
	});

	render() {
		return (
			<Fragment>
				<Image {...this.props} />
				<div className="loading" data-test="loading">
					<Dropdown
						placeholder="Loading..."
						fluid
						search
						selection
						options={[]}
						loading
					/>
				</div>
			</Fragment>
		);
	}
}

export default Loading;
