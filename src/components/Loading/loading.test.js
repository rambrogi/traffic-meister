import React from "react";
import ReactDOM from "react-dom";
import renderer from "react-test-renderer";
import { shallow, mount, render } from "enzyme";

import { mockData } from "../../utils/test-utils";
import data from "../../utils/data";
import Loading from "./index";

it("should render without throwing an error", () => {
	expect(shallow(<Loading image="" transition={jest.fn()} />).length).toEqual(
		1
	);
});

it("should call API", async () => {
	expect.assertions(1);
	const wrapper = shallow(<Loading image="" transition={jest.fn()} />);

	try {
		await wrapper.instance().fetchData();
		expect(wrapper).toMatchSnapshot();
	} catch (err) {
		expect(wrapper).toMatchSnapshot();
	}
});

it("should simulate API failure", () => {
	const wrapper = shallow(<Loading image="" transition={jest.fn()} />);

	expect(wrapper).toMatchSnapshot();
	wrapper.instance().transitionFail("Error");
	expect(wrapper).toMatchSnapshot();
});
