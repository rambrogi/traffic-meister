import React from "react";
import ReactDOM from "react-dom";
import renderer from "react-test-renderer";
import { shallow, mount, render } from "enzyme";

import App from "./index";

it("should render without throwing an error", () => {
	expect(shallow(<App />).length).toEqual(1);
});
