import React from "react";
import ReactDOM from "react-dom";
import renderer from "react-test-renderer";
import { shallow, mount, render } from "enzyme";

import { mockData } from "../../utils/test-utils";
import data from "../../utils/data";
import Image from "./index";

it("should render without throwing an error", () => {
	expect(shallow(<Image image="" transition={jest.fn()} />).length).toEqual(
		1
	);
});

it("should fail", async () => {
	expect.assertions(1);
	const wrapper = shallow(<Image image="fasd" transition={jest.fn()} />);

	wrapper.instance().handleError();
	expect(wrapper).toMatchSnapshot();
});
