import React from "react";
import { string, func } from "prop-types";
import "./style.css";

/**
 * Shows Image above the dropdowns
 *
 * @class ImageComponent
 * @extends {React.Component}
 */
class ImageComponent extends React.Component {
	static propTypes = {
		image: string.isRequired,
		transition: func.isRequired
	};

	/**
	 * fallback image
	 *
	 * @memberof ImageComponent
	 */
	handleError = () => {
		this.props.transition("SELECT", {
			setState: {
				image:
					"https://cdn.importantmedia.org/planetsave/uploads/2011/12/26090623/traffic.jpg"
			}
		});
	};

	render() {
		return (
			<div className="image" data-test="image">
				<img
					alt="TrafficMeister Inc."
					className="image__img"
					src={this.props.image}
					onError={this.handleError}
				/>
			</div>
		);
	}
}

export default ImageComponent;
