import React, { Component } from "react";
import { oneOfType, string, object, func, number } from "prop-types";
import { Dropdown } from "semantic-ui-react";

import "./style.css";

/**
 * Color dropdown
 *
 * @class Colors
 * @extends {Component}
 */
class Colors extends Component {
	static propTypes = {
		data: object.isRequired,
		types: string,
		transition: func.isRequired,
		brand: oneOfType([string, number]),
		color: string
	};

	getItem(item) {
		return this.props.data.get(item);
	}

	/**
	 * transition to next screen after a color
	 * is selected
	 *
	 * @memberof Colors
	 */
	selectColor = (e, { value }) => {
		const checkType = this.getItem("types");
		const { types, transition } = this.props;

		const getType = types && checkType.get(types);
		const hasColor = getType ? getType.has(value) : true;

		const options = {
			setState: {
				color: value,
				types: hasColor ? types : ""
			}
		};
		transition("NEXT", options);
	};

	/**
	 * returns an array with all
	 * valid colors
	 *
	 * @returns {array}
	 * @memberof Colors
	 */
	renderColorList() {
		const list = [];
		const colors = this.getItem("colors");
		const vehicles = this.getItem("vehicles");

		this.pushToList(vehicles, list, colors);

		return list;
	}

	/**
	 * if a brand is selected display colors
	 * available to that brand only. if a type is
	 * selected, display colors available to that type,
	 * if nothing. display all colors
	 *
	 * @param {object} vehicles - Vehicles List
	 * @param {array} list  - Color Options
	 * @param {object} colors  - Color List
	 * @memberof Colors
	 */
	pushToList(vehicles, list, colors) {
		const { typeSelected, nothingSelected } = this.checkSelected();

		this.props.brand && this.brandList(vehicles, list);
		typeSelected && this.typeList(list);
		nothingSelected && this.newList(colors, list);
	}

	/**
	 * check what field is already
	 * selected
	 *
	 * @returns  {object}
	 * @memberof Colors
	 */
	checkSelected() {
		const { brand, types } = this.props;
		const typeSelected = !brand && types;
		const nothingSelected = !brand && !types;
		return { typeSelected, nothingSelected };
	}

	/**
	 * method to handle list when nothing
	 * is selected
	 *
	 * @param {any} colors - List of the Colors
	 * @param {any} list - Color options
	 * @memberof Colors
	 */
	newList(colors, list) {
		colors.forEach(item => {
			list.push(this.getListItem(item));
		});
	}

	/**
	 * returns an object with the formatted
	 * item option
	 *
	 * @param {string} item - The Color name
	 * @returns {object}
	 * @memberof Colors
	 */
	getListItem(item) {
		return {
			key: item,
			value: item,
			text: item,
			label: {
				circular: true,
				color: item === "white" ? null : item,
				empty: true
			}
		};
	}

	/**
	 * Represents the list when type is
	 * selected
	 *
	 * @param {array} list - The filtered colors by Type
	 * @memberof Colors
	 */
	typeList(list) {
		const getTypes = this.getItem("types");
		const getColors = getTypes.get(this.props.types);
		getColors.forEach(item => {
			list.push(this.getListItem(item));
		});
	}

	/**
	 * Represents the list when a brand
	 * is selected
	 *
	 * @param {object} vehicles - The Vehicles
	 * @param {array} list - The options
	 * @memberof Colors
	 */
	brandList(vehicles, list) {
		const vehicle = vehicles.get(Number(this.props.brand));
		vehicle.colors.forEach(item => {
			list.push(this.getListItem(item));
		});
	}

	render() {
		return (
			<div className="colors" data-test="colors">
				<Dropdown
					data-test="colors-input"
					placeholder="Select a Color"
					fluid
					search
					selection
					onChange={this.selectColor}
					options={this.renderColorList()}
					labeled
					value={this.props.color}
				/>
			</div>
		);
	}
}

export default Colors;
