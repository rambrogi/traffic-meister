import React from "react";
import ReactDOM from "react-dom";
import renderer from "react-test-renderer";
import { shallow, mount, render } from "enzyme";

import { mockData } from "../../../utils/test-utils";
import Colors from "./index";

describe("Colors Component", () => {
	it("should render without throwing an error", () => {
		expect(
			shallow(<Colors data={mockData} transition={jest.fn()} />).length
		).toEqual(1);
	});

	it("should response to event change when color isn't selected", () => {
		const wrapper = shallow(
			<Colors data={mockData} types="car" transition={jest.fn()} />
		);

		expect(wrapper).toMatchSnapshot();
		wrapper.instance().selectColor(null, {
			value: "black"
		});
		expect(wrapper).toMatchSnapshot();
	});

	describe("Filter values on brand/type change", () => {
		it("has only brand selected", () => {
			const wrapper = shallow(
				<Colors
					brand={99}
					color="black"
					data={mockData}
					transition={jest.fn()}
				/>
			);

			expect(wrapper).toMatchSnapshot();
			wrapper.instance().selectColor(null, {
				value: "black"
			});
			expect(wrapper).toMatchSnapshot();
		});
		it("has only type selected", () => {
			const wrapper = shallow(
				<Colors
					color="black"
					types="car"
					data={mockData}
					transition={jest.fn()}
				/>
			);

			expect(wrapper).toMatchSnapshot();
			wrapper.instance().selectColor(null, {
				value: "black"
			});
			expect(wrapper).toMatchSnapshot();
		});
		it("has both selected", () => {
			const wrapper = shallow(
				<Colors
					color="black"
					brand={99}
					types="car"
					data={mockData}
					transition={jest.fn()}
				/>
			);

			expect(wrapper).toMatchSnapshot();
			wrapper.instance().selectColor(null, {
				value: "black"
			});
			expect(wrapper).toMatchSnapshot();
		});
		it("has a color not available to a type", () => {
			const wrapper = shallow(
				<Colors types="car" data={mockData} transition={jest.fn()} />
			);

			expect(wrapper).toMatchSnapshot();
			wrapper.instance().selectColor(null, {
				value: "yellow"
			});
			expect(wrapper).toMatchSnapshot();
		});
	});
});
