import React, { Component, Fragment } from "react";
import { State } from "react-gizmo";

import Image from "../Image";
import Colors from "./Colors";
import BrandsAndTypes from "./BrandsAndTypes";

class Vehicles extends Component {
	render() {
		return (
			<Fragment>
				<Image {...this.props} />
				<Colors {...this.props} />
				<State on="brandsAndTypes">
					<BrandsAndTypes />
				</State>
			</Fragment>
		);
	}
}

export default Vehicles;
