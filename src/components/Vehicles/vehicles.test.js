import React from "react";
import ReactDOM from "react-dom";
import renderer from "react-test-renderer";
import { shallow, mount, render } from "enzyme";

import { mockData } from "../../utils/test-utils";
import Vehicles from "./index";

it("should render without throwing an error", () => {
	expect(
		shallow(<Vehicles image="" data={mockData} transition={jest.fn()} />)
			.length
	).toEqual(1);
});
