import React, { Component, Fragment } from "react";
import { object, func } from "prop-types";

import "./style.css";
import Brands from "./Brands";
import Types from "./Types";

/**
 * Component Wrapper for the Brands and
 * Types
 *
 * @class BrandsAndTypes
 * @extends {Component}
 */
class BrandsAndTypes extends Component {
	static propTypes = {
		data: object,
		transition: func
	};
	/**
	 * method to handle type and brand input
	 * change. if it's the brand input, change
	 * the image to the selected one
	 *
	 * @param {object} e
	 * @param {object} data
	 */
	handleChange = (e, data) => {
		const { name, value } = data;
		let options = { setState: { [name]: value } };

		if (name === "brand") {
			const vehicles = this.props.data.get("vehicles");
			const vehicle = vehicles.get(value);
			options = { setState: { ...options.setState, image: vehicle.img } };
		}

		this.props.transition("SELECT", options);
	};
	render() {
		return (
			<Fragment>
				<div className="brands-and-types" data-test="brands">
					<Brands handleChange={this.handleChange} {...this.props} />
				</div>
				<div className="brands-and-types" data-test="types">
					<Types handleChange={this.handleChange} {...this.props} />
				</div>
			</Fragment>
		);
	}
}

export default BrandsAndTypes;
