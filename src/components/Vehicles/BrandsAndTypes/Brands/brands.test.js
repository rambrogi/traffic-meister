import React from "react";
import ReactDOM from "react-dom";
import renderer from "react-test-renderer";
import { shallow, mount, render } from "enzyme";

import { mockData } from "../../../../utils/test-utils";
import Brands from "./index";

describe("Brands Component", () => {
	it("should render without throwing an error", () => {
		expect(
			shallow(
				<Brands
					data={mockData}
					color="black"
					transition={jest.fn()}
					handleChange={jest.fn()}
				/>
			).find("[data-test='brands-input']").length
		).toEqual(1);
	});

	it("displays correctly", () => {
		const tree = renderer
			.create(
				<Brands
					data={mockData}
					color="black"
					transition={jest.fn()}
					handleChange={jest.fn()}
				/>
			)
			.toJSON();
		expect(tree).toMatchSnapshot();
	});

	it("should respond to event change", () => {
		const wrapper = shallow(
			<Brands
				data={mockData}
				color="black"
				transition={jest.fn()}
				handleChange={jest.fn()}
				types="car"
			/>
		);
		const input = wrapper.find("[data-test='brands-input']");

		expect(wrapper).toMatchSnapshot();
		input.simulate("change", { data: { name: "brand", value: 99 } });
		expect(wrapper).toMatchSnapshot();
	});
});
