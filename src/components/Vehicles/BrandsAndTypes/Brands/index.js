import React, { Component } from "react";
import { Dropdown, Icon } from "semantic-ui-react";
import { oneOfType, number, string, func, object } from "prop-types";

class Brands extends Component {
	static propTypes = {
		data: object.isRequired,
		color: string.isRequired,
		types: string,
		brand: oneOfType([number, string]),
		transition: func.isRequired,
		handleChange: func.isRequired
	};

	/**
	 * retrieve the vehicles and return an array
	 * with the options for the <Dropdown>
	 *
	 * @returns array
	 */
	renderBrandList = () => {
		const vehicles = this.props.data.get("vehicles");
		const list = [];

		this.createList(vehicles, list);

		return list;
	};

	/**
	 * check if brands have the selected color.
	 * and if brands type matches with selected type if any.
	 *
	 * @param {object} item - Brand Item
	 */
	checkValidity = item =>
		item.colors.includes(this.props.color) && this.checkTypes(item);

	/**
	 * check if brand type is equal to the selected type
	 * return true if none is selected
	 *
	 * @param {any} item - Brand Item
	 */
	checkTypes = item =>
		this.props.types ? item.type === this.props.types : true;

	/**
	 * map through each vehicle and check if that
	 * vehicle is valid (color and type). if its,
	 * push vehicle to brand list.
	 *
	 * @param {object} vehicles - Vehicles List
	 * @param {array} list - Brands options
	 */
	createList = (vehicles, list) => {
		const icons = {
			car: "car",
			airplane: "plane",
			train: "train"
		};
		vehicles.forEach(item => {
			const options = {
				key: item.id,
				value: item.id,
				text: item.brand,
				icon: <Icon name={icons[item.type]} />
			};
			this.checkValidity(item) && list.push(options);
		});
	};

	render() {
		return (
			<Dropdown
				data-test="brands-input"
				name="brand"
				placeholder="Select a Brand"
				fluid
				search
				selection
				onChange={this.props.handleChange}
				options={this.renderBrandList()}
				labeled
				value={this.props.brand}
			/>
		);
	}
}

export default Brands;
