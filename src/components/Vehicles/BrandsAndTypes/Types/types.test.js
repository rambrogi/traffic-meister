import React from "react";
import ReactDOM from "react-dom";
import renderer from "react-test-renderer";
import { shallow, mount, render } from "enzyme";

import { mockData } from "../../../../utils/test-utils";
import Types from "./index";

describe("Types Component", () => {
	it("should render without throwing an error", () => {
		expect(
			shallow(
				<Types
					data={mockData}
					color="black"
					transition={jest.fn()}
					handleChange={jest.fn()}
				/>
			).find("[data-test='types-input']").length
		).toEqual(1);
	});

	it("displays correctly", () => {
		const tree = renderer
			.create(
				<Types
					data={mockData}
					color="black"
					transition={jest.fn()}
					handleChange={jest.fn()}
				/>
			)
			.toJSON();
		expect(tree).toMatchSnapshot();
	});

	it("should respond to event change", () => {
		const wrapper = shallow(
			<Types
				data={mockData}
				color="black"
				transition={jest.fn()}
				handleChange={jest.fn()}
				brand="99"
			/>
		);
		const input = wrapper.find("[data-test='types-input']");

		expect(wrapper).toMatchSnapshot();
		input.simulate("change", { data: { types: "car" } });
		expect(wrapper).toMatchSnapshot();
	});
});
