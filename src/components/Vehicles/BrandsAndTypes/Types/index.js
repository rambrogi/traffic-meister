import React, { Component } from "react";
import { Dropdown, Icon } from "semantic-ui-react";
import { string, func, oneOfType, number, object } from "prop-types";

/**
 * Types Dropdown
 *
 * @class Types
 * @extends {Component}
 */
class Types extends Component {
	static propTypes = {
		data: object.isRequired,
		brand: oneOfType([string, number]),
		color: string.isRequired,
		handleChange: func.isRequired,
		types: string,
		transition: func.isRequired
	};

	/**
	 * creates the list with valid types filtering
	 * types if a brand is already selected
	 *
	 * @returns array
	 */
	renderTypeList = () => {
		const list = [];
		const { brand, data } = this.props;
		const vehicles = data.get("vehicles");
		const types = data.get("types");

		if (brand) this.hasBrandSelected(vehicles, list);
		else this.createOptionList(types, list);

		return list;
	};

	/**
	 * check the relationship between type and color
	 * if valid push the type item to the array
	 *
	 * @param {object} types - The Types
	 * @param {array} list - Types Options
	 * @memberof Types
	 */
	createOptionList(types, list) {
		for (const [key] of types) {
			const colors = types.get(key);
			colors.has(this.props.color) && list.push(this.listItem(key));
		}
	}

	/**
	 * option item
	 *
	 * @param {string} key - Single type
	 * @returns object
	 * @memberof Types
	 */
	listItem(key) {
		const icons = {
			car: "car",
			airplane: "plane",
			train: "train"
		};

		return {
			key: key,
			value: key,
			text: key,
			icon: <Icon name={icons[key]} />
		};
	}

	/**
	 * when there is a brand selected, set the available
	 * types to only its type
	 *
	 * @param {object} vehicles - The Vehicles
	 * @param {array} list - Types Options
	 * @memberof Types
	 */
	hasBrandSelected(vehicles, list) {
		const vehicle = vehicles.get(Number(this.props.brand));
		list.push(this.listItem(vehicle.type));
	}

	render() {
		return (
			<Dropdown
				data-test="types-input"
				name="types"
				placeholder="Select a Type"
				fluid
				search
				selection
				onChange={this.props.handleChange}
				options={this.renderTypeList()}
				labeled
				value={this.props.types}
			/>
		);
	}
}

export default Types;
