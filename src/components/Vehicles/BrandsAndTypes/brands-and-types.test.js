import React from "react";
import ReactDOM from "react-dom";
import renderer from "react-test-renderer";
import { shallow, mount, render } from "enzyme";

import { mockData } from "../../../utils/test-utils";
import BrandsAndTypes from "./index";

describe("BrandsAndTypes Component", () => {
	it("should render without throwing an error", () => {
		expect(
			shallow(
				<BrandsAndTypes
					color="black"
					data={mockData}
					transition={jest.fn()}
				/>
			).length
		).toEqual(1);
	});

	it("displays correctly", () => {
		const tree = renderer
			.create(
				<BrandsAndTypes
					color="black"
					data={mockData}
					transition={jest.fn()}
				/>
			)
			.toJSON();
		expect(tree).toMatchSnapshot();
	});

	describe("should respond to event change", () => {
		it("is a brand", () => {
			const wrapper = shallow(
				<BrandsAndTypes
					color="black"
					data={mockData}
					transition={jest.fn()}
				/>
			);

			expect(wrapper).toMatchSnapshot();
			wrapper.instance().handleChange(null, {
				name: "brand",
				value: 99
			});
			expect(wrapper).toMatchSnapshot();
		});

		it("is a type", () => {
			const wrapper = shallow(
				<BrandsAndTypes
					color="black"
					data={mockData}
					transition={jest.fn()}
				/>
			);

			expect(wrapper).toMatchSnapshot();
			wrapper.instance().handleChange(null, {
				name: "type",
				value: "car"
			});
			expect(wrapper).toMatchSnapshot();
		});
	});
});
