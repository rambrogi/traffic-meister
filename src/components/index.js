import React from "react";
import { State } from "react-gizmo";
import { Segment } from "semantic-ui-react";

import "./style.css";
import Loading from "./Loading";
import Vehicles from "./Vehicles";
import FormResult from "./FormResult";
import ErrorComponent from "./Error";

const App = () => (
	<div className="app">
		<Segment className="app__wrapper">
			<State on="loading">
				<Loading />
			</State>
			<State on="colors">
				<Vehicles />
			</State>
			<State on="error">
				<ErrorComponent />
			</State>
		</Segment>
		<State on="colors">
			<FormResult />
		</State>
	</div>
);

export default App;
