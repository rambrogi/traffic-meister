import React, { Component } from "react";
import { string, func, object } from "prop-types";
import { Button, Icon, Segment } from "semantic-ui-react";
import "./style.css";

/**
 * Component responsible for displaying
 * the value of the selected fields
 * in the bottom
 *
 * @class FormResult
 * @extends {Component}
 */
class FormResult extends Component {
	static propTypes = {
		color: string,
		types: string,
		transition: func,
		data: object
	};

	/**
	 * returns the name of selected vehicle
	 * or default message if none
	 *
	 * @memberof FormResult
	 */
	getBrandMessage = () =>
		this.props.brand ? this.findBrand() : "No Brand Selected";

	/**
	 * after the reset button is clicked.
	 * this method resets all fields
	 * to their initial state
	 *
	 * @memberof FormResult
	 */
	resetData = () => {
		const options = {
			setState: {
				color: "",
				brand: "",
				types: "",
				image:
					"https://cdn.importantmedia.org/planetsave/uploads/2011/12/26090623/traffic.jpg"
			},
			off: ["brandsAndTypes", "colors"]
		};
		this.props.transition("RESET", options);
	};

	findBrand() {
		const vehicles = this.props.data.get("vehicles");
		const vehicle = vehicles.get(Number(this.props.brand));
		return vehicle.brand;
	}

	render() {
		return (
			<Segment className="form-result" data-test="form-result">
				<p data-test="form-result-color">
					Color: {this.props.color || "No Color Selected"}
				</p>
				<p data-test="form-result-brand">
					Brand: {this.getBrandMessage()}
				</p>
				<p data-test="form-result-type">
					Type: {this.props.types || "No Type Selected"}
				</p>
				<Button
					primary
					fluid
					onClick={this.resetData}
					data-test="form-result-button"
				>
					<Icon name="repeat" /> Reset
				</Button>
			</Segment>
		);
	}
}

export default FormResult;
