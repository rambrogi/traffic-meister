import React from "react";
import ReactDOM from "react-dom";
import renderer from "react-test-renderer";
import { shallow, mount, render } from "enzyme";

import { mockData } from "../../utils/test-utils";
import FormResult from "./index";

describe("FormResult Component", () => {
	it("should render without throwing an error", () => {
		expect(
			shallow(<FormResult />).find("[data-test='form-result']").length
		).toEqual(1);
	});
	it("display correct results", () => {
		const wrapper = shallow(<FormResult />);
		const getNode = id => wrapper.find(`[data-test='${id}']`);

		expect(getNode("form-result-color").length).toEqual(1);
		expect(getNode("form-result-brand").length).toEqual(1);
		expect(getNode("form-result-type").length).toEqual(1);

		expect(getNode("form-result-color").text()).toEqual(
			"Color: No Color Selected"
		);
		expect(getNode("form-result-brand").text()).toEqual(
			"Brand: No Brand Selected"
		);
		expect(getNode("form-result-type").text()).toEqual(
			"Type: No Type Selected"
		);
		wrapper.setProps({
			color: "black",
			data: mockData,
			brand: 99,
			types: "car"
		});
		expect(getNode("form-result-color").text()).toEqual("Color: black");
		expect(getNode("form-result-brand").text()).toEqual("Brand: f40");
		expect(getNode("form-result-type").text()).toEqual("Type: car");
	});

	describe("Reset Button", () => {
		it("should not throw error on click", () => {
			const transition = jest.fn();
			const wrapper = shallow(<FormResult transition={transition} />);
			const btn = wrapper.find("[data-test='form-result-button']");

			btn.simulate("click");
			expect(transition).toHaveBeenCalledTimes(1);
		});
	});

	it("renders", () => {
		const tree = renderer.create(<FormResult />).toJSON();
		expect(tree).toMatchSnapshot();
	});
});
