import React from "react";
import ReactDOM from "react-dom";
import renderer from "react-test-renderer";
import { shallow, mount, render } from "enzyme";

import ErrorComponent from "./index";

describe("Error Component", () => {
	it("renders", () => {
		const tree = renderer.create(<ErrorComponent />).toJSON();
		expect(tree).toMatchSnapshot();
	});
});
