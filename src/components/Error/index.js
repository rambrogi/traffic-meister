import React from "react";
import { Button, Message, Icon } from "semantic-ui-react";
import { compose, withHandlers } from "recompose";
import video from "../../assets/crash.mp4";
import "./style.css";

const enhance = compose(
	withHandlers({
		handleRetry: props => () => props.transition("RETRY", { off: "error" })
	})
);

const ErrorComponent = ({ handleRetry }) => (
	<div data-test="error">
		<div className="video-wrapper">
			<video src={video} autoPlay loop />
		</div>
		<Message negative>
			<p>
				Something wrong happened. Looks like the monkeys ran out of
				bananas.{" "}
				<span aria-label="monkey-bananas" role="img">
					🙈🍌
				</span>
			</p>
		</Message>
		<Button
			primary
			fluid
			onClick={handleRetry}
			data-test="form-result-button"
		>
			<Icon name="repeat" /> Retry
		</Button>
	</div>
);

export default enhance(ErrorComponent);
